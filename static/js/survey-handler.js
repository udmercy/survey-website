Survey.StylesManager.applyTheme("modern");

function resultHandlerBuilder(name) {
  return function (result) {
    fetch("/api/results/" + name, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(result.data),
    })
      .then((response) => response.json())
      .then((data) => {
        var elem = document.createElement("h3");
        elem.innerHTML = "Your survey completion code is: " + data.code;
        var completed = document.getElementsByClassName("sv-completedpage")[0];
        completed.appendChild(elem);
      })
      .catch((err) => {
        var elem = document.createElement("h3");
        elem.innerHTML = "Your survey completion code is: 999960";
        var completed = document.getElementsByClassName("sv-completedpage")[0];
        completed.appendChild(elem);
      });
  };
}

function uploadFileHandlerBuilder() {
  return async function (survey, options) {
    var data = options.files[0];
    var text = await data.text();
    var err = await fetch("/api/commodity/parse-profile-html", {
      method: "POST",
      body: text,
    })
      .then((response) => response.json())
      .then((parsed) => {
        if (parsed.length == 0) {
            alert("Your ad settings could not be found in the file you uploaded.");
            return parsed;
        }
        var sliced = parsed.slice();
        var added = [];
        for (i = 1; i <= 12; i++) {
          var page = window.survey.getPageByName("property" + i);
          if (i > parsed.length) {
            survey.removePage(page);
          } else {
            var random = Math.floor(Math.random() * sliced.length);
            page.title = ("Google thinks that you are interested in: " + sliced[random]);
            page.getQuestionByName("label").html = sliced[random];
            page.getQuestionByName("property" + i + " matrix").clearValue();
            added.push(sliced[random]);
            sliced.splice(random, 1);
          }
        }
        //var final = window.survey.getPageByName("select");
        //final.getQuestionByName("selectfromall").choices = parsed.slice();
        survey.setValue('topics', added);
        console.log(parsed);
        return null;
      })
      .catch((error) => {
        console.error(error);
        alert("Your ad settings could not be found in the file you uploaded.");
        return error;
      });

    if (err != null) {
      if (!data.name.toLowerCase().endsWith('.html')) {
        options.question.addError(new Survey.CustomError("The file you upload must be an HTML file. Please refer to the instructions above and try again."));
      } else {
        options.question.addError(new Survey.CustomError("You may have uploaded a different HTML file. Please refer to the instructions above and try again."));
      }
      options.callback("error");
      return;
    }

    options.callback("success", [
      {
        file: data,
        content: text,
      },
    ]);
  };
}

function load_survey(name) {
  form_promise = fetch("/surveys/" + name + ".json");

  window.onload = function () {
    form_promise
      .then((res) => res.json())
      .then((out) => {
        json = out;
        window.survey = new Survey.Model(json);
        survey.render("surveyElement");
        survey.onComplete.add(resultHandlerBuilder(name));
        survey.onUploadFiles.add(uploadFileHandlerBuilder());
      })
      .catch((err) => (window.location = "/404"));
  };
}
